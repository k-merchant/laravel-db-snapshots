<?php

namespace Spatie\DbSnapshots\Commands;

use Illuminate\Console\Command;
use Spatie\DbSnapshots\SnapshotRepository;
use Spatie\DbSnapshots\Commands\Concerns\AsksForSnapshotName;

class Delete extends Command
{
    use AsksForSnapshotName;

    protected $signature = 'snapshot:delete {name?} {--exploded}';

    protected $description = 'Delete a snapshot.';

    public function handle()
    {
        $path = config('filesystems.disks.snapshots.root');
        if(!$this->option('exploded')) {

            if (app(SnapshotRepository::class)->getFiles()->isEmpty()) {
                $this->warn('No snapshots found. Run `snapshot:create` to create snapshots.');

                return;
            }

            $name = $this->argument('name') ?: $this->askForSnapshotName();

            $snapshot = app(SnapshotRepository::class)->findByName($name);

            if (!$snapshot) {
                $this->warn("Snapshot `{$name}` does not exist!");
                return;
            }

            if ($conf = $this->confirm('Do you wish to delete snapshot:' . $name . ' ?')) {
                $folder = $path . '/'. $name;
                if (!empty(\File::isDirectory($folder))) {
                    \File::deleteDirectory($folder);
                }
                $snapshot->delete();
                return $this->info("Snapshot `{$snapshot->name}` deleted!");
            } else {
                return $this->info('aborted ...');
            }
        } else {
            $folders = \File::directories($path);
            $splited = array_map(array($this,'mod'),$folders);
            if ( empty($folders) ) {
                $this->info('No exploded snapshots .');
            } else {
                $this->info('Following folders will be deleted:');
                $this->table('Local path', $splited);
                if ($conf = $this->confirm('Do you wish to proceed with deletion ?')) {
                    foreach($folders as $folder) {
                        if (!empty(\File::isDirectory($folder))) {
                            \File::deleteDirectory($folder);
                        }
                    }
                    return $this->info('Done ...');
                } else {
                    return $this->info('aborted ...');
                }

            }
        }

    }
    private function mod($n) {
        return array($n);

    }
}
