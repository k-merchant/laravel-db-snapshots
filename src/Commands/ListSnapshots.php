<?php

namespace Spatie\DbSnapshots\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Spatie\DbSnapshots\Snapshot;
use Spatie\DbSnapshots\Helpers\Format;
use Spatie\DbSnapshots\SnapshotRepository;
use Symfony\Component\Console\Helper\TableSeparator;
use Spatie\DbSnapshots\Commands\Concerns\AsksForSnapshotName;

class ListSnapshots extends Command
{
    protected $signature = 'snapshot:list {--all} {--explode}';

    protected $description = 'List all the snapshots.';
    use AsksForSnapshotName;
    use ConfirmableTrait;

    public function handle()
    {

        if (!$this->option('all') || $this->option('explode')) {
            $snapshots = app(SnapshotRepository::class)->getFiles();
        } else {
            $snapshots = app(SnapshotRepository::class)->getAll();
        }



        if (!$this->option('explode')) {
            if ($snapshots->isEmpty()) {
                $this->warn('No snapshots found. Run `snapshot:create` to create one.');

                return;
            }
            $totalSize = 0;

            if(!$this->option('all')) {
                $rows = $snapshots->map(function (Snapshot $snapshot) use (&$totalSize) {
                    $totalSize += $snapshot->size();
                    return [
                        $snapshot->name,
                        $snapshot->createdAt()->format('Y-m-d H:i:s'),
                        Format::humanReadableSize($snapshot->size()),
                    ];
                });
                $rows->push(new TableSeparator);
                $rows->push(['Total size','',Format::humanReadableSize($totalSize)]);
                $this->table(['Name', 'Created at', 'Size'], $rows);
            } else {
                $folders = [];
                $rows = $snapshots->map(function (Snapshot $snapshot) use (&$folders, &$totalSize) {
                    $folderName = ($snapshot->fileName == ($snapshot->name.'.sql')) ? '' : substr($snapshot->fileName, 0, strpos($snapshot->fileName, '/'));
                    $toDisplay = '';
                    $folderSize = 0;
                    $totalSize += $snapshot->size();
                    if (!empty($folderName) && !in_array($folderName, $folders)) {
                        $folderSize = $this->folderSize($folderName);
                        $toDisplay = ' ' . $folderName . ' ';
                        array_push($folders, $folderName);
                    } else if(empty($folderName)){
                        $toDisplay = ' Full Backup ';
                    }

                    return [
                        ((!empty($folderSize)) ?
                            "<bg=green;fg=red;bold>" :
                            ( (!empty($toDisplay)) ?
                                "<bg=yellow;fg=red;bold>" :
                                '') ).$toDisplay."</>",
                        $snapshot->name,
                        $snapshot->createdAt()->format('Y-m-d H:i:s'),
                        Format::humanReadableSize($snapshot->size()),
                        (!empty($folderSize)) ? "<bg=green;fg=red;bold> " . Format::humanReadableSize($folderSize) . " </>" : ''
                    ];
                });

                $rows->push(new TableSeparator);
                $rows->push(['<bg=green;fg=red;bold> Total size </>','', '', '', '<bg=green;fg=red;bold> ' . Format::humanReadableSize($totalSize) . ' </>']);
                $this->table(["Directory\nBackup Time", 'Name', 'Created at', 'Size', 'Folder Size'], $rows);
            }

        } else {

            $name = $this->askForSnapshotName();
            $path = config('filesystems.disks.snapshots.root');
            $folder = $path . '/'. $name;
            if (!empty(\File::isDirectory($folder))) {
                $this->error(' Folder already exists ');
            }
            if ($conf = $this->confirm('Do you wish to continue to extract:' . $name . ' ?')) {
                $this->info('Selected backup: '. $name);
                $fileName = $name . '.sql';
                if (!empty($path)) {
                    $file = $path . '/' . $fileName;
                    if (file_exists($file)) {
                        $this->info('Split result: ' . $this->SplitSQL($file) . ' files created.') ;
                    }
                }
            }

        }
    }

    private function SplitSQL($file, $delimiter = ';')
    {
        set_time_limit(0);
        $folder = str_replace('.sql', '', $file);
        if (is_file($file) === true)
        {
            $file = fopen($file, 'r');

            if (is_resource($file) === true)
            {
                $query = array();
                $regexpDrop = '/DROP TABLE IF EXISTS\s+`(\w+)`/';
                $regexpUnlock = '/UNLOCK TABLES/';

                if (empty(\File::isDirectory($folder))) {
                    \File::MakeDirectory($folder, 0755, true);
                } else {

                    $time_start = microtime(true);

                    \File::cleanDirectory($folder);

                    echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);

                }
                $writeTo = $folder . '/others.sql';
                while (feof($file) === false)
                {
                    $query[] = fgets($file);

                    if (preg_match('~' . preg_quote($delimiter, '~') . '\s*$~iS', end($query)) === 1)
                    {
                        $query = trim(implode('', $query));

                        if (\DB::query($query) === false)
                        {
                            echo '<h3>ERROR: ' . $query . '</h3>' . "\n";
                        }
                        else
                        {
                            preg_match_all($regexpDrop, $query, $matches);
                            preg_match_all($regexpUnlock, $query, $unlocks);
                            if(!empty($matches[1]) && !empty($matches[1][0])){
                                $writeTo = $folder . '/' . $matches[1][0] . '.sql';
                                file_put_contents($writeTo, $query. PHP_EOL, FILE_APPEND | LOCK_EX);
                            } else if(!empty($unlocks[0]) && !empty($unlocks[0][0])) {
                                file_put_contents($writeTo, $query. PHP_EOL, FILE_APPEND | LOCK_EX);
                                $writeTo = $folder . '/others.sql';
                            } else {
                                file_put_contents($writeTo, $query. PHP_EOL, FILE_APPEND | LOCK_EX);
                            }
                        }
                    }

                    if (is_string($query) === true)
                    {
                        $query = array();
                    }
                }

                if(fclose($file)){
                    return count(\File::allFiles($folder));
                }
                return 0;
            }
        }

        return false;
    }

    private function folderSize($path)
    {
        $path = config('filesystems.disks.snapshots.root') . '/' . $path;

        $file_size = 0;

        foreach( \File::allFiles($path) as $file)
        {
            $file_size += $file->getSize();
        }
        return $file_size;
    }

}
