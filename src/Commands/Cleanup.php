<?php

namespace Spatie\DbSnapshots\Commands;

use Illuminate\Console\Command;
use Spatie\DbSnapshots\SnapshotRepository;

class Cleanup extends Command
{
    protected $signature = 'snapshot:cleanup {--keep=}';

    protected $description = 'Specify how many snapshots to keep and delete the rest';

    public function handle()
    {
        $path = config('filesystems.disks.snapshots.root');

        $snapshots = app(SnapshotRepository::class)->getFiles();

        $keep = $this->option('keep');

        if (! $this->option('keep')) {
            $this->warn('No value for option --keep.');

            return;
        }

        $snapshots->splice($keep)->each(function ($snapshot) use ($path) {
            $folder = $path . '/' . $snapshot->name;
            if (!empty(\File::isDirectory($folder))) {
                \File::deleteDirectory($folder);
            }
            $snapshot->delete();
        });
        $this->info('Done');
    }
}
